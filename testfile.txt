Let me make it clear that I am not happy .
I think he likes it .
He said it would take three days to clear it all away .
Would it be appropriate for you , Madam President , to write a letter to the Sri Lankan President expressing Parliament 's regret at his and the other violent deaths in Sri Lanka and urging her to do everything she possibly can to seek a peaceful reconciliation to a very difficult situation ?
It says that this should be done despite the principle of relative stability .
I believe that the principle of relative stability is a fundamental legal principle of the common fisheries policy and a proposal to subvert it would be legally inadmissible .
Indeed , it is quite in keeping with the positions this House has always adopted .
It is the case of Alexander Nikitin .
All of us here are pleased that the courts have acquitted him and made it clear that in Russia , too , access to environmental information is a constitutional right .
We know , and we have stated as much in very many resolutions indeed , including specifically during the last plenary part-session of last year , that this is not solely a legal case and that it is wrong for Alexander Nikitin to be accused of criminal activity and treason because of our involvement as the beneficiaries of his findings .
These findings form the basis of the European programmes to protect the Barents Sea , and that is why I would ask you to examine a draft letter setting out the most important facts and to make Parliament 's position , as expressed in the resolutions which it has adopted , clear as far as Russia is concerned .
It will , I hope , be examined in a positive light .
Madam President , can you tell me why this Parliament does not adhere to the health and safety legislation that it actually passes ?
It seems absolutely disgraceful that we pass legislation and do not adhere to it ourselves .
We shall therefore look into it properly to ensure that everything is as it should be .
It is irresponsible of EU Member States to refuse to renew the embargo .
I would like Mr Barón Crespo , who made the request , to speak to propose it . That is , if he so wishes , of course .
I would also like to point out , Madam President , that this Parliament voted to express its confidence in President Prodi during the previous legislature . It did so again during this legislature , in July , and then , in September , it voted once more to approve the whole Commission .
There has therefore been enough time for the Commission to prepare its programme and for us to become familiar with it and explain it to our citizens .
The events of last week - which originated outside the Conference of Presidents , that Conference being used simply to corroborate and ratify decisions taken elsewhere - present us with a dilemma . Either the Commission is not ready to present this programme , in which case it should clarify it .
According to its President , it is in a position to do so .
Given that the Commission is represented by Vice-President de Palacio , I believe that , before voting , it would help if the Commission could let us know how ready it is to present this programme , as agreed . Alternatively , Parliament is not ready to examine this programme , as some appear to be suggesting .
In my opinion , this second hypothesis would imply the failure of Parliament in its duty as a Parliament , as well as introducing an original thesis , an unknown method which consists of making political groups aware , in writing , of a speech concerning the Commission ' s programme a week earlier - and not a day earlier , as had been agreed - bearing in mind that the legislative programme will be discussed in February , so we could forego the debate , since on the next day our citizens will hear about it in the press and on the Internet and Parliament will no longer have to worry about it .
